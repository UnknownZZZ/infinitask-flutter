part of 'task_list_view_bloc.dart';


abstract class TaskListViewState extends Equatable{
  const TaskListViewState();
  @override

  List<Object?> get props => [];


}

class TaskListViewInitialState extends TaskListViewState{

  const TaskListViewInitialState();
  @override
  List<Object?> get props => [];

}


class TaskListViewLoadingState extends TaskListViewState{
  const TaskListViewLoadingState();
  @override
  List<Object?> get props => [];
}


class TaskListViewLoadedState extends TaskListViewState{
  final List<Day> days;
  final int currentPage;
  const TaskListViewLoadedState({required this.days, required this.currentPage});
  @override
  List<Object?> get props => [days,currentPage];
}



class SingleDayPageLoadInfoState extends TaskListViewState{
final String date;
  const SingleDayPageLoadInfoState({required this.date});
  @override
  List<Object?> get props => [date];
}



