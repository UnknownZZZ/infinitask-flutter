
import 'package:equatable/equatable.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinitask/models/day.dart';
import 'package:infinitask/services/repository/event/event_repository.dart';

part  'task_list_view_event.dart';
part 'task_list_view_state.dart';

class TaskListViewBloc extends Bloc<TaskListViewEvent, TaskListViewState>{

  final EventRepository eventRepository;

  TaskListViewBloc({required this.eventRepository}) : super(const TaskListViewInitialState(),){

    on<TaskListViewStartEvent>((event, emit) async{
      final List<Day> days =[];
      emit(TaskListViewLoadingState());
      final Map<String, dynamic> allEvents = await eventRepository.getAllPaginated(page: 1);

      allEvents['response'].forEach((singleEvent) {
        final Day dayItem = Day(
          currentDate: singleEvent['currentDate'].toString(),
          taskCount: singleEvent['taskCount'],
        );
        days.add(dayItem);
      });
      emit(TaskListViewLoadedState(days: days,currentPage: 1));
    });





    on<TaskListPageLoadByPageEvent>((event, emit) async{
      //print('paginate data');
      final currentPage = (state as TaskListViewLoadedState).currentPage;
      final currentDays = (state as TaskListViewLoadedState).days;
      print(currentPage);
      final List<Day> days =[];
      //emit(TaskListViewLoadingState());
      final Map<String, dynamic> allEvents = await eventRepository.getAllPaginated(page: currentPage+1);

      allEvents['response'].forEach((singleEvent) {
        final Day dayItem = Day(
          currentDate: singleEvent['currentDate'].toString(),
          taskCount: singleEvent['taskCount'],
        );
        days.add(dayItem);
      });
      currentDays.addAll(days);

      emit(TaskListViewLoadedState(days: currentDays,currentPage: currentPage+1));
    });



    on<SingleDayPageLoadInfoEvent>((event, emit)  {
      emit(SingleDayPageLoadInfoState(date: event.date));
    });


  }

}