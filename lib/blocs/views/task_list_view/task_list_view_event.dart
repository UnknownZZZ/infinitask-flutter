part of 'task_list_view_bloc.dart';


abstract class TaskListViewEvent extends Equatable{
  const TaskListViewEvent();
  @override

  List<Object?> get props => [];

}

class TaskListViewStartEvent extends TaskListViewEvent{}


class SingleDayPageLoadInfoEvent extends TaskListViewEvent{
  final String date;
  const SingleDayPageLoadInfoEvent({required this.date});
  List<Object?> get props => [date];
}


class TaskListPageLoadByPageEvent extends TaskListViewEvent{
  final int page;
  final List<Day> days;
  const TaskListPageLoadByPageEvent({required this.page, required this.days});
  List<Object?> get props => [page,days];
}



