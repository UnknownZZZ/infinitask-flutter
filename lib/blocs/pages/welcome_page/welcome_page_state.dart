part of 'welcome_page_bloc.dart';


abstract class WelcomePageState extends Equatable{
  const WelcomePageState();
  @override

  List<Object?> get props => [];


}

class WelcomePageInitialState extends WelcomePageState{
  @override
  List<Object?> get props => [];

}

class WelcomePageAuthorizedState extends WelcomePageState{
  @override
  List<Object?> get props => [];
}
