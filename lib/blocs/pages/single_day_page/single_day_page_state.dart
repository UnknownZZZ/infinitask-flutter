part of 'single_day_page_bloc.dart';


abstract class SingleDayPageState extends Equatable{
  const SingleDayPageState();
  @override

  List<Object?> get props => [];


}

class SingleDayPageInitialState extends SingleDayPageState{
  @override
  List<Object?> get props => [];

}

class SingleDayTasksLoadedState extends SingleDayPageState{
  final List<Event> tasks;
  const SingleDayTasksLoadedState({required this.tasks});
  @override
  List<Object?> get props => [tasks];
}

class SingleDayTasksLoadingState extends SingleDayPageState{

  const SingleDayTasksLoadingState();
  @override
  List<Object?> get props => [];
}

class SingleDayTasksEmptyState extends SingleDayPageState{

  const SingleDayTasksEmptyState();
  @override
  List<Object?> get props => [];
}


class SingleDayTasksRemovingState extends SingleDayPageState{

  const SingleDayTasksRemovingState();
  @override
  List<Object?> get props => [];
}
class SingleDayTasksRemovedState extends SingleDayPageState{

  const SingleDayTasksRemovedState();
  @override
  List<Object?> get props => [];
}


class SingleDayTasksFinishingState extends SingleDayPageState{
  const SingleDayTasksFinishingState();
  @override
  List<Object?> get props => [];
}
class SingleDayTasksFinishedState extends SingleDayPageState{
  const SingleDayTasksFinishedState();
  @override
  List<Object?> get props => [];
}