part of 'single_day_page_bloc.dart';


abstract class SingleDayPageEvent extends Equatable{
  const SingleDayPageEvent();
  @override

  List<Object?> get props => [];

}


class SingleDayPageStartEvent extends SingleDayPageEvent{
  final String date;
  const SingleDayPageStartEvent({required this.date});
  List<Object?> get props => [date];
}
class SingleDayPageRemoveTaskEvent extends SingleDayPageEvent{
  final String id;
  const SingleDayPageRemoveTaskEvent({required this.id});
  List<Object?> get props => [id];
}

class SingleDayPageDoneTaskEvent extends SingleDayPageEvent{
  final String id;
  const SingleDayPageDoneTaskEvent({required this.id});
  List<Object?> get props => [id];
}

class SingleDayPageLoadingEvent extends SingleDayPageEvent{
  const SingleDayPageLoadingEvent();

  List<Object?> get props => [];
}


