
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinitask/models/event.dart';
import 'package:infinitask/services/repository/event/event_repository.dart';

part  'single_day_page_event.dart';
part 'single_day_page_state.dart';

class SingleDayPageBloc extends Bloc<SingleDayPageEvent, SingleDayPageState>{
  final EventRepository eventRepository;

  SingleDayPageBloc({required this.eventRepository}) : super(
    SingleDayPageInitialState(),
  ){


    on<SingleDayPageStartEvent>((event, emit) async{

      final List<Event> tasks =[];
     emit(SingleDayTasksLoadingState());
      final Map<String, dynamic> allTasks = await eventRepository.getAllByDate(date: event.date);

      allTasks['response'].forEach((singleTask) {
        final Event eventItem = Event(
          id: singleTask['id'],
          task_date: singleTask['task_date'],
          task_title: singleTask['task_title'],
          task_description: singleTask['task_description'],
          status: singleTask['task_status']
        );
        tasks.add(eventItem);

      });

      if(tasks.length > 0){
        emit(SingleDayTasksLoadedState(tasks: tasks));
      }
      if(tasks.length == 0){
        emit(SingleDayTasksEmptyState());

      }

    });


    on<SingleDayPageRemoveTaskEvent>((event, emit) async{
      emit(SingleDayTasksRemovingState());
      final  apiResult = await eventRepository.removeTaskById(id: event.id);
      emit(SingleDayTasksRemovedState());
    });


    on<SingleDayPageDoneTaskEvent>((event, emit) async{
      emit(SingleDayTasksFinishingState());
      final  apiResult = await eventRepository.doneTaskById(id: event.id);
      emit(SingleDayTasksFinishedState());
    });






  }

}