part of 'new_task_page_bloc.dart';


abstract class NewTaskPageState extends Equatable{
  const NewTaskPageState();
  @override

  List<Object?> get props => [];


}

class NewTaskPageInitialState extends NewTaskPageState{
  @override
  List<Object?> get props => [];

}

class NewTaskPageStartState extends NewTaskPageState{

  const NewTaskPageStartState();
  @override
  List<Object?> get props => [];
}

class AddNewTaskPendingState extends NewTaskPageState{

  const AddNewTaskPendingState();
  @override
  List<Object?> get props => [];
}

class AddNewTaskDoneState extends NewTaskPageState{

  const AddNewTaskDoneState();
  @override
  List<Object?> get props => [];
}





