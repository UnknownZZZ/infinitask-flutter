part of 'new_task_page_bloc.dart';


abstract class NewTaskPageEvent extends Equatable{
  const NewTaskPageEvent();
  @override

  List<Object?> get props => [];

}


class NewTaskPageStartEvent extends NewTaskPageEvent{

  const NewTaskPageStartEvent();
  List<Object?> get props => [];
}
class NewTaskPageAddNewTaskEvent extends NewTaskPageEvent{
final String date;
final String taskTitle;
final String taskDescription;
  const NewTaskPageAddNewTaskEvent({required this.date,required this.taskTitle, required this.taskDescription});
  List<Object?> get props => [];
}



