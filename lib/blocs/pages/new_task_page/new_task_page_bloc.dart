
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinitask/models/event.dart';
import 'package:infinitask/services/repository/event/event_repository.dart';

part  'new_task_page_event.dart';
part 'new_task_page_state.dart';

class NewTaskPageBloc extends Bloc<NewTaskPageEvent, NewTaskPageState>{
  final EventRepository eventRepository;

  NewTaskPageBloc({required this.eventRepository}) : super(
    NewTaskPageInitialState(),
  ){


    on<NewTaskPageStartEvent>((event, emit) async{


    });

    on<NewTaskPageAddNewTaskEvent>((event, emit) async{
      emit(AddNewTaskPendingState());
      final Map<String, dynamic> createTaskResult = await eventRepository.createNewTask(date: event.date, title: event.taskTitle, description: event.taskDescription);
      emit(AddNewTaskDoneState());
      //print(createTaskResult);
    });








  }

}