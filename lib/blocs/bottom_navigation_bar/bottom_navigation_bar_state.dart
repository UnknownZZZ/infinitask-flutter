part of 'bottom_navigation_bar_bloc.dart';


abstract class BottomNavigationState extends Equatable{
  const BottomNavigationState();
  @override

  List<Object?> get props => [];


}

class BottomNavigationInitialState extends BottomNavigationState{
  final int index = 0;

  @override
  List<Object?> get props => [index];

}

class TaskListViewState extends BottomNavigationState{
  final int index = 0;

  @override
  List<Object?> get props => [index];

}



class ProfileViewState extends BottomNavigationState{
  final int index = 2;

  @override
  List<Object?> get props => [index];

}

