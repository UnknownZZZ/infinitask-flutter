part of 'bottom_navigation_bar_bloc.dart';


abstract class BottomNavigationEvent extends Equatable{
  const BottomNavigationEvent();
  @override

  List<Object?> get props => [];

}

class BottomNavigationStartEvent extends BottomNavigationEvent{

}
class BottomNavigationLoadTaskListEvent extends BottomNavigationEvent{}
class BottomNavigationLoadMoreEvent extends BottomNavigationEvent{}


