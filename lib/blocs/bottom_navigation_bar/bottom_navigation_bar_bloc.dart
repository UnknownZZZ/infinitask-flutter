
import 'package:equatable/equatable.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
part  'bottom_navigation_bar_event.dart';
part 'bottom_navigation_bar_state.dart';

class BottomNavigationBloc extends Bloc<BottomNavigationEvent, BottomNavigationState>{

  BottomNavigationBloc() : super(
     BottomNavigationInitialState(),
  ){
    on<BottomNavigationStartEvent>((event, emit) {
      emit(BottomNavigationInitialState());
    });

    on<BottomNavigationLoadTaskListEvent>((event, emit) {
      emit(TaskListViewState());
    });

    on<BottomNavigationLoadMoreEvent>((event, emit) {
      emit(ProfileViewState());
    });


  }

}