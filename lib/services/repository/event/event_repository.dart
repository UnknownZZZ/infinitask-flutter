import 'event_api_provider.dart';

class EventRepository{
  final EventApiProvider _eventApiProvider = EventApiProvider();

  getAllPaginated({page}) => _eventApiProvider.getAllPaginated(page);
  getAllByDate({date}) => _eventApiProvider.getAllByDate(date);
  createNewTask({date,title,description}) => _eventApiProvider.createNewTask(date,title,description);
  removeTaskById({id}) => _eventApiProvider.removeTaskById(id);
  doneTaskById({id}) => _eventApiProvider.doneTaskById(id);

}