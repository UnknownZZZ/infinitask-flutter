import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:infinitask/constants/constants.dart' as constants;
class EventApiProvider{

  Future  getAllPaginated(int page) async{
    final response = await http.get(
      Uri.parse(constants.apiBaseURL+"api/task/getAllPaginated?page="+page.toString()),
      headers: {'Accept':'application/json'},
    );
    if(response.body.isNotEmpty){
      final apiResult = jsonDecode(response.body);
      return apiResult;
    }
    return "undefined error";
  }

  Future  getAllByDate(String date) async{
    final response = await http.get(
      Uri.parse(constants.apiBaseURL+"api/task/getAllByDate?date="+date),
      headers: {'Accept':'application/json'},
    );
    if(response.body.isNotEmpty){
      final apiResult = jsonDecode(response.body);
      return apiResult;
    }
    return "undefined error";
  }

  Future  createNewTask(String date, String title, String description) async{
    final response = await http.post(
      Uri.parse(constants.apiBaseURL+"api/task"),
      headers: {'Accept':'application/json'},
      body: {'date':date, 'title':title, 'description':description},
    );
    if(response.body.isNotEmpty){
      final apiResult = jsonDecode(response.body);
      return apiResult;
    }
    return "undefined error";
  }

  Future  removeTaskById(String id) async{
    final response = await http.delete(
      Uri.parse(constants.apiBaseURL+"api/task"),
      headers: {'Accept':'application/json'},
      body: {'id':id},
    );
    if(response.body.isNotEmpty){
      final apiResult = jsonDecode(response.body);
      return apiResult;
    }
    return "undefined error";
  }

  Future  doneTaskById(String id) async{
    final response = await http.post(
      Uri.parse(constants.apiBaseURL+"api/task/markasdone"),
      headers: {'Accept':'application/json'},
      body: {'id':id},
    );
    if(response.body.isNotEmpty){
      final apiResult = jsonDecode(response.body);
      return apiResult;
    }
    return "undefined error";
  }


}