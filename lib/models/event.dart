class Event{
  final int id;
  final String task_date;
  final String task_title;
  final String task_description;
  final String status;

  Event({
    required this.id,
    required this.task_date,
    required this.task_title,
    required this.task_description,
    required this.status,

  });
}