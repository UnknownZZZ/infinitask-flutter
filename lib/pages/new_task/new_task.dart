import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinitask/blocs/pages/new_task_page/new_task_page_bloc.dart';
import 'package:infinitask/blocs/pages/single_day_page/single_day_page_bloc.dart';
import 'package:infinitask/services/repository/event/event_repository.dart';

class NewTaskPage extends StatelessWidget {
  final String date;
  final _formKey = GlobalKey<FormState>();
  final taskTitle = TextEditingController();
  final taskDescription = TextEditingController();

  NewTaskPage({required this.date}) : super();
  final eventRepository = EventRepository();
  @override
  Widget build(BuildContext context) {

    return BlocProvider(

      create:(context)=>NewTaskPageBloc(eventRepository: eventRepository)..add(NewTaskPageStartEvent()),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: IconButton(
            icon: const Icon(Icons.keyboard_backspace_rounded, color: Colors.black,size: 30,),
            onPressed: () => Navigator.of(context).pop(),
          ),


        ),
        body: Column(
          children: [
            BlocBuilder<NewTaskPageBloc, NewTaskPageState>(
              builder: (context,state){
                return Container(
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Card(
                          margin: EdgeInsets.zero,
                          elevation: 0,
                          child: ListTile(

                            title: Text(date,
                            ),
                            subtitle: const Text('Нове завдання',style: const TextStyle(fontSize: 25, fontWeight: FontWeight
                                .bold,color: Colors.black),),
                            //trailing: Icon(Icons.more_vert),
                          ),
                        ),
                        Container(
                          height: 20,
                          color: Colors.white,
                        ),





                      ],
                    )
                );
              },
            ),
            BlocBuilder<NewTaskPageBloc, NewTaskPageState>(
              builder: (context,state){

                if(state is AddNewTaskDoneState){

                  SchedulerBinding.instance!.addPostFrameCallback((_) {
                    Navigator.pop(context,true);

                  });
                }
                return Expanded(

                  child: Form(
                    key: _formKey,
                    child: Column(

                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: SingleChildScrollView(
                            padding: EdgeInsets.only(top: 15, bottom: 15,left: 15, right: 15),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Назва завдання (300 сим.)',style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black),),
                                SizedBox(height: 5,),
                                TextFormField(
                                  controller: taskTitle,
                                  decoration: InputDecoration(
                                    contentPadding:
                                    const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
                                    filled: true,
                                    fillColor: Colors.white,
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(10),
                                    ),

                                    hintText: 'Назва завдання',

                                  ),
                                  validator: (String? value) {
                                    return (value == null || value.isEmpty) ? 'Обов\'язкове поле' : null;
                                  },
                                ),

                                SizedBox(height: 50,),

                                Text('Опис завдання (300 сим.)',style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black),),
                                SizedBox(height: 5,),
                                TextFormField(
                                  minLines: 8,
                                  maxLines: 8,
                                  controller: taskDescription,
                                  decoration: InputDecoration(

                                    contentPadding:
                                    const EdgeInsets.only(left: 14.0, bottom: 14.0, top: 14.0),
                                    filled: true,
                                    fillColor: Colors.white,
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(10),
                                    ),

                                    hintText: 'Опис завдання',

                                  ),
                                  validator: (String? value) {
                                    return (value == null || value.isEmpty) ? 'Обов\'язкове поле' : null;
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 15, bottom: 15,left: 15, right: 15),
                          height: 100,
                          child: Container(
                            margin: const EdgeInsets.only(bottom: 15.0),
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(

                              gradient: const LinearGradient(
                                  begin: Alignment(0.5, -1.0),
                                  end: Alignment(-0.5, 1.0),
                                  stops: [0.1, 0.2, 0.9],
                                  colors: [Color(0xff9C9ED7), Color(0xff9C9ED7), Color(0xff7C7FC0),]
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child:TextButton(
                              style: TextButton.styleFrom(
                                primary: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(40.0),
                                ),
                              ),
                              onPressed: () {
                                if(_formKey.currentState!.validate()){
                                  context.read<NewTaskPageBloc>().add(NewTaskPageAddNewTaskEvent(date:date, taskTitle: taskTitle.text, taskDescription: taskDescription.text));
                                }

                                //Navigator.push(context, CupertinoPageRoute(builder: (context) =>  NewTaskPage(date: date,)));
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  (state is AddNewTaskPendingState)
                                      ? Container(
                                      width: 20,
                                      height: 20,
                                      margin: EdgeInsets.only(right: 10),
                                      child: const CircularProgressIndicator(color: Colors.white,)
                                  )
                                      : Container(),
                                  Text("Додати завдання",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );

              },
            )
          ],
        ),
      ),
    );
  }
}
