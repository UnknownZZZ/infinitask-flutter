import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinitask/blocs/pages/welcome_page/welcome_page_bloc.dart';
import 'package:infinitask/pages/home_page/home_page.dart';


class WelcomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return BlocProvider(

      create:(context) => WelcomePageBloc()..add(WelcomePageStartEvent()),
      child: BlocBuilder<WelcomePageBloc, WelcomePageState>(
        builder: (context, state){


          return Scaffold(
            body: Container(

              alignment: Alignment.bottomLeft,
              decoration: const BoxDecoration(
                color: Color(0xffE0F0FE),

              ),
              child: Container(

                padding: EdgeInsets.only(left: 30, right: 30, bottom: 30),

                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    Image(image: AssetImage('assets/images/mainimage.png')),

                    const SizedBox(height: 30,),
                    Text('Керуй задачами протягом дня',style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Color(0xff5350CB)),),
                    const SizedBox(height: 30,),
                    Text('Додавай задачі, які потрібно виконати в декілька кліків!', style: TextStyle( color: Color(0xff5350CB)),),
                    SizedBox(height: 30,),
                    Stack(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(bottom: 15.0),
                          width: 50,
                          height: 50,

                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.3),
                                  spreadRadius: 5,
                                  blurRadius: 20,
                                  offset: Offset(0, 3), // changes position of shadow
                                ),
                              ],
                              borderRadius: BorderRadius.circular(50),
                              color: Colors.white

                          ),

                        ),

                        TextButton(
                          style: TextButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(40.0),
                            ),
                          ),
                          onPressed: () {

                            Navigator.push(context, CupertinoPageRoute(builder: (context) =>  HomePage()));
                          },
                          child: Text("Розпочати",style: TextStyle(color: Color(0xff5350CB), fontWeight: FontWeight.bold),),
                        )
                      ],
                    ),
                    SizedBox(height: 20,),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

