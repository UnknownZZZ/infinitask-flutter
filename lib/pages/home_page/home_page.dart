import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinitask/blocs/bottom_navigation_bar/bottom_navigation_bar_bloc.dart';
import 'package:infinitask/pages/home_page/views/my_profile.dart';
import 'package:infinitask/pages/home_page/views/my_tasks.dart';
import 'package:infinitask/pages/new_task/new_task.dart';
import 'package:intl/intl.dart';


class HomePage extends StatelessWidget {
  //const HomePage({Key? key}) : super(key: key);

  final List<Widget> screens = [
    TaskListView(),
    Container(),
    ProfileView(),
  ];

  @override
  Widget build(BuildContext context) {

    return BlocProvider(
      create: (context) => BottomNavigationBloc()..add(BottomNavigationStartEvent()),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          automaticallyImplyLeading: false,
          elevation: 0,
        ),
        body: BlocBuilder<BottomNavigationBloc, BottomNavigationState>(
            builder: (context,state){
              if(state is BottomNavigationInitialState){
                return screens[state.index];
              }
              if(state is TaskListViewState){
                return screens[state.index];
              }

              if(state is ProfileViewState){
                return screens[state.index];
              }
              return screens[0];
            }),



        bottomNavigationBar: BlocBuilder<BottomNavigationBloc, BottomNavigationState>(
          builder: (context,state){
            return Material(
              elevation: 15,
              child: Stack(

                alignment: Alignment.center,
                children: [
                  Container(
                    height: 100,

                    alignment: Alignment.center,
                    color: Colors.white,
                    //padding: EdgeInsets.only(top: 15),
                    child: BottomAppBar(
                      elevation: 0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          InkWell(
                            borderRadius: BorderRadius.circular(25),
                              child: Container(
                                height:50,
                                width: 50,
                                  child:
                                  (state is BottomNavigationInitialState) ?
                                  Icon(Icons.task_outlined,size: 30, color: Color(0xff5350CB),) :
                                  (state is TaskListViewState) ?
                                  Icon(Icons.task_outlined,size: 30, color: Color(0xff5350CB),) :
                                  Icon(Icons.task_outlined,size: 30,),
                              ),
                              onTap: (){context.read<BottomNavigationBloc>().add(BottomNavigationLoadTaskListEvent());},
                          ),
                          Container(
                            width: 50,
                            height: 50,
                            child: FloatingActionButton(

                              onPressed: () {

                                // Add your onPressed code here!
                                Navigator.push(context, MaterialPageRoute(builder: (context) =>  NewTaskPage(date: DateFormat('yyyy/MM/dd').format(DateTime.now()),)));
                              },

                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)
                              ),
                              backgroundColor: Color(0xffA8ABDE),
                              child: const Icon(Icons.add,color: Colors.white,),
                            ),
                          ),
                          InkWell(
                            borderRadius: BorderRadius.circular(25),
                            child:  Container(

                                height:50,
                                width: 50,
                                child:(state is ProfileViewState) ?
                                Icon(Icons.person_outlined,size: 30, color: Color(0xff5350CB),) :
                                Icon(Icons.person_outlined,size: 30,),

                            ),
                            onTap: (){context.read<BottomNavigationBloc>().add(BottomNavigationLoadMoreEvent());},
                          ),



                        ],
                      ),
                    ),
                  ),

                ],
              ),
            );
          },
        ),
      ),

    );

  }



}
