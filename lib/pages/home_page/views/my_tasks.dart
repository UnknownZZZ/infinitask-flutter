import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:infinitask/blocs/views/task_list_view/task_list_view_bloc.dart';
import 'package:infinitask/pages/single_day_page/single_day_page.dart';
import 'package:infinitask/services/repository/event/event_repository.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:intl/intl.dart' as intl;
import 'package:skeleton_loader/skeleton_loader.dart';
class TaskListView extends StatelessWidget {
  TaskListView({Key? key}) : super(key: key);
  final eventRepository = EventRepository();
  final RefreshController refreshController = RefreshController();
  @override
  Widget build(BuildContext context) {

    return BlocProvider(
      create:(context) => TaskListViewBloc(eventRepository: eventRepository)..add(TaskListViewStartEvent()),
      child: Column(
        children: [
          BlocBuilder<TaskListViewBloc, TaskListViewState>(
            builder: (context, state){

              return Container(
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const ListTile(
                        leading: CircleAvatar(
                            backgroundColor: Color(0xffe6e6e6),
                            radius: 50,
                            child: Icon(Icons.person, size: 50, color: Color(
                                0xff5350CB),)
                        ),
                        title: Text('Привіт!',
                          style: TextStyle(fontSize: 25, fontWeight: FontWeight
                              .bold),),
                        subtitle: Text('Гарного дня :)'),
                        //trailing: Icon(Icons.more_vert),
                      ),
                      Container(
                        height: 50,
                        color: Colors.white,
                      ),
                      Container(

                        padding: const EdgeInsets.only(
                            left: 15, right: 15, bottom: 15),
                        child: const Text('Мої завдання', style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.bold),),
                      ),
                    ],
                  )
              );

            },
          ),
          BlocBuilder<TaskListViewBloc, TaskListViewState>(
            builder: (context, state){

              if(state is TaskListViewLoadingState){

                return  Expanded(

                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      CircularProgressIndicator(),
                      SizedBox(
                        height: 10,
                      ),
                      Text("Завантаження завдань"),
                    ],
                  ),
                );
              }
              if(state is SingleDayPageLoadInfoState){
                SchedulerBinding.instance!.addPostFrameCallback((_) {
                  Navigator.push(context, MaterialPageRoute(builder: (context) =>  SingleDayPage(date: state.date,)));
                });
              }
              if(state is TaskListViewLoadedState) {
                return Expanded(

                    child: Container(
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: SmartRefresher(
                              enablePullUp: true,
                              controller: refreshController,
                              footer: const ClassicFooter(
                                  loadStyle: LoadStyle.ShowWhenLoading),
                              onRefresh: (){
                                print('refresh data');
                                context.read<TaskListViewBloc>().add(TaskListViewStartEvent());
                                refreshController.refreshCompleted();
                              },
                              onLoading: () async{

                                print(state.currentPage+1);
                                context.read<TaskListViewBloc>().add(TaskListPageLoadByPageEvent(page: state.currentPage+1,days: state.days));
                                refreshController.loadComplete();
                                //refreshController.loadNoData();
                                //print('no data');
                              },
                              child: ListView.builder(
                                itemCount: state.days.length,
                                padding: const EdgeInsets.only(
                                    left: 15.0, right: 15.0, top: 15),
                                itemBuilder: (context, position) {
                                  return GestureDetector(

                                    child: Card(
                                      elevation: 0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(25),
                                      ),
                                      color: (state.days[position].taskCount==0)? Color(0xffF1F2FF) : Color(0xff9C9ED7),
                                      margin: const EdgeInsets.only(bottom: 15),
                                      child: ListTile(
                                        contentPadding: EdgeInsets.only(
                                            top: 10, bottom: 10, left: 15, right: 15),
                                        leading: Container(
                                            width: 50,
                                            height: 50,

                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(50),

                                              color:  Color(0xff403EA3),
                                            ),
                                            child: const Icon(
                                              Icons.date_range_outlined, size: 30,
                                              color: Color(0xffffffff),)

                                        ),
                                        title:
                                        (state.days[position].taskCount==0) ?
                                        const Text('Завдань немає', style: TextStyle(fontSize: 18,
                                            fontWeight: FontWeight.bold,
                                            color: Color(0xff161543)),)
                                            :
                                        Text("Записів: "+state.days[position].taskCount.toString(), style: TextStyle(fontSize: 18,
                                            fontWeight: FontWeight.bold,
                                            color: Color(0xff161543)),),


                                        subtitle:  Text(state.days[position].currentDate),

                                      ),
                                    ),
                                    onTap: (){
                                      Navigator.push(context, MaterialPageRoute(builder: (context) =>  SingleDayPage(date: state.days[position].currentDate,)));
                                      //context.read<TaskListViewBloc>().add(SingleDayPageLoadInfoEvent(date: state.days[position].currentDate));
                                    },


                                  );
                                },
                              ),

                            ),
                          ),

                        ],
                      ),
                    )
                );
              }
              return Container();

            },
          ),
        ],
      ),
    );


  }

}
