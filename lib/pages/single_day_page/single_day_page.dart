import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinitask/blocs/pages/new_task_page/new_task_page_bloc.dart';
import 'package:infinitask/blocs/pages/single_day_page/single_day_page_bloc.dart';
import 'package:infinitask/blocs/views/task_list_view/task_list_view_bloc.dart';
import 'package:infinitask/pages/new_task/new_task.dart';
import 'package:infinitask/services/repository/event/event_repository.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class SingleDayPage extends StatelessWidget {
  final String date;
   SingleDayPage({required this.date}) : super();

  final eventRepository = EventRepository();
  @override
  Widget build(BuildContext context) {

    //final newTaskPageBloc = BlocProvider.of<NewTaskPageBloc>(context);
    return BlocProvider(

      create:(context)=>SingleDayPageBloc(eventRepository: eventRepository)..add(SingleDayPageStartEvent(date: date)),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
            elevation: 0,
            leading: IconButton(
              icon: const Icon(Icons.keyboard_backspace_rounded, color: Colors.black,size: 30,),
              onPressed: () => Navigator.of(context).pop(),
            ),

        ),
        body: Column(
          children: [
            BlocBuilder<SingleDayPageBloc, SingleDayPageState>(
              builder: (context,state){
                return Container(
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ListTile(
                          title: Text(date,),
                          subtitle: const Text('Завдання на день',style: const TextStyle(fontSize: 25, fontWeight: FontWeight
                              .bold,color: Colors.black),),
                        ),
                        Container(
                          height: 50,
                          color: Colors.white,
                        ),
                      ],
                    )
                );
              },
            ),
            BlocBuilder<SingleDayPageBloc, SingleDayPageState>(
              builder: (context,state){

                if(state is SingleDayTasksFinishingState){

                  return Expanded(

                    child: Column(

                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 15, bottom: 15,left: 15, right: 15),
                          height: 100,
                          child: Container(
                            margin: const EdgeInsets.only(bottom: 15.0),
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(

                              gradient: const LinearGradient(
                                  begin: Alignment(0.5, -1.0),
                                  end: Alignment(-0.5, 1.0),
                                  stops: [0.1, 0.2, 0.9],
                                  colors: [Color(0xff9C9ED7), Color(0xff9C9ED7), Color(0xff7C7FC0),]
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child:TextButton(
                              style: TextButton.styleFrom(
                                primary: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(40.0),
                                ),
                              ),
                              onPressed: () {
                                Navigator.push(context, CupertinoPageRoute(builder: (context) =>  NewTaskPage(date: date,)));
                                //context.read<WelcomePageBloc>().add(WelcomePageStartEvent());
                                //Navigator.push(context, CupertinoPageRoute(builder: (context) =>  AuthPage()));
                              },
                              child: Text("Додати завдання",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                }
                if(state is SingleDayTasksFinishedState){
                  context.read<SingleDayPageBloc>().add(SingleDayPageStartEvent(date: date));
                }
                if(state is SingleDayTasksRemovingState){

                  return Expanded(

                    child: Column(

                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 15, bottom: 15,left: 15, right: 15),
                          height: 100,
                          child: Container(
                            margin: const EdgeInsets.only(bottom: 15.0),
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(

                              gradient: const LinearGradient(
                                  begin: Alignment(0.5, -1.0),
                                  end: Alignment(-0.5, 1.0),
                                  stops: [0.1, 0.2, 0.9],
                                  colors: [Color(0xff9C9ED7), Color(0xff9C9ED7), Color(0xff7C7FC0),]
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child:TextButton(
                              style: TextButton.styleFrom(
                                primary: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(40.0),
                                ),
                              ),
                              onPressed: () {
                                Navigator.push(context, CupertinoPageRoute(builder: (context) =>  NewTaskPage(date: date,)));
                                //context.read<WelcomePageBloc>().add(WelcomePageStartEvent());
                                //Navigator.push(context, CupertinoPageRoute(builder: (context) =>  AuthPage()));
                              },
                              child: Text("Додати завдання",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                }
                if(state is SingleDayTasksRemovedState){
                  context.read<SingleDayPageBloc>().add(SingleDayPageStartEvent(date: date));
                }
                if(state is SingleDayTasksLoadingState){

                  return Expanded(

                    child: Column(

                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 15, bottom: 15,left: 15, right: 15),
                          height: 100,
                          child: Container(
                            margin: const EdgeInsets.only(bottom: 15.0),
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(

                              gradient: const LinearGradient(
                                  begin: Alignment(0.5, -1.0),
                                  end: Alignment(-0.5, 1.0),
                                  stops: [0.1, 0.2, 0.9],
                                  colors: [Color(0xff9C9ED7), Color(0xff9C9ED7), Color(0xff7C7FC0),]
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child:TextButton(
                              style: TextButton.styleFrom(
                                primary: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(40.0),
                                ),
                              ),
                              onPressed: () {
                                Navigator.push(context, CupertinoPageRoute(builder: (context) =>  NewTaskPage(date: date,)));
                                //context.read<WelcomePageBloc>().add(WelcomePageStartEvent());
                                //Navigator.push(context, CupertinoPageRoute(builder: (context) =>  AuthPage()));
                              },
                              child: Text("Додати завдання",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                }
                if(state is SingleDayTasksLoadedState){

                  return Expanded(

                    child: Column(

                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: ListView.builder(
                            itemCount: state.tasks.length,
                            padding: const EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 15),
                            itemBuilder: (context, position) {
                              return GestureDetector(
                                child: Card(
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25),
                                  ),
                                  color: (state.tasks[position].status == 'new')? const Color(0xff36348E) : const Color(0xffF1F2FF),
                                  margin: const EdgeInsets.only(bottom: 15),
                                  child:  Slidable(
                                    key: const ValueKey(0),
                                    endActionPane: ActionPane(
                                      // A motion is a widget used to control how the pane animates.
                                      motion: const ScrollMotion(),

                                      // A pane can dismiss the Slidable.
                                      dismissible: DismissiblePane(onDismissed: () {}),

                                      // All actions are defined in the children parameter.
                                      children:  [
                                        // A SlidableAction can have an icon and/or a label.
                                        SlidableAction(
                                          onPressed: (context) => {
                                            context.read<SingleDayPageBloc>().add(SingleDayPageRemoveTaskEvent(id: state.tasks[position].id.toString()))
                                          },
                                          backgroundColor: Color(0xFFFE4A49),
                                          foregroundColor: Colors.white,
                                          icon: Icons.delete,
                                          //label: 'Видалити',
                                        ),

                                        SlidableAction(
                                          onPressed: (context) => {
                                            context.read<SingleDayPageBloc>().add(SingleDayPageDoneTaskEvent(id: state.tasks[position].id.toString()))
                                          },
                                          backgroundColor: Color(0xFF7CC083),
                                          foregroundColor: Colors.white,
                                          icon: Icons.check,
                                          //label: 'Завершити',
                                        ),
                                      ],
                                    ),
                                    child: ListTile(
                                      contentPadding: const EdgeInsets.only(
                                          top: 15, bottom: 15, left: 25, right: 25),

                                      title:

                                      Padding(
                                        padding: const EdgeInsets.only(bottom: 15),
                                        child: Text(state.tasks[position].task_title, style:  TextStyle(fontSize: 18,
                                          fontWeight: FontWeight.bold,

                                          color: (state.tasks[position].status == 'new') ? const Color(0xffffffff) : const Color(0xff6e6e6e),
                                          decoration: (state.tasks[position].status == 'new') ? TextDecoration.none : TextDecoration.lineThrough,
                                          //color: Color(0xff161543)),
                                        ),
                                        ),
                                      ),


                                      subtitle:  Text(state.tasks[position].task_description,
                                        style: TextStyle(
                                          color: (state.tasks[position].status == 'new') ? const Color(0xffffffff) : const Color(0xff6e6e6e),
                                          decoration: (state.tasks[position].status == 'new') ? TextDecoration.none : TextDecoration.lineThrough,
                                        ),
                                      ),

                                    ),
                                  ),
                                ),
                                onTap: (){
                                  //context.read<SingleDayPageBloc>().add(SingleDayPageRemoveTaskEvent(id: state.tasks[position].id.toString()));
                                  //context.read<TaskListViewBloc>().add(SingleDayPageLoadInfoEvent(date: state.tasks[position].currentDate));
                                },
                              );
                            },
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 15, bottom: 15,left: 15, right: 15),
                          height: 100,
                          child: Container(
                            margin: const EdgeInsets.only(bottom: 15.0),
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(

                              gradient: const LinearGradient(
                                  begin: Alignment(0.5, -1.0),
                                  end: Alignment(-0.5, 1.0),
                                  stops: [0.1, 0.2, 0.9],
                                  colors: [Color(0xff9C9ED7), Color(0xff9C9ED7), Color(0xff7C7FC0),]
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child:TextButton(
                              style: TextButton.styleFrom(
                                primary: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(40.0),
                                ),
                              ),
                              onPressed: () async {
                                await Navigator.push(context, CupertinoPageRoute(builder: (context) =>  NewTaskPage(date: date,)));
                                context.read<SingleDayPageBloc>().add(SingleDayPageStartEvent(date: date));
                                //context.read<WelcomePageBloc>().add(WelcomePageStartEvent());
                                //Navigator.push(context, CupertinoPageRoute(builder: (context) =>  AuthPage()));
                              },
                              child: Text("Додати завдання",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                }
                if(state is SingleDayTasksEmptyState){

                  return Expanded(

                    child: Column(

                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: Center(
                            child: Text('Завдань немає'),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 15, bottom: 15,left: 15, right: 15),
                          height: 100,
                          child: Container(
                          margin: const EdgeInsets.only(bottom: 15.0),
                          height: 50,
                            width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(

                            gradient: const LinearGradient(
                                begin: Alignment(0.5, -1.0),
                                end: Alignment(-0.5, 1.0),
                                stops: [0.1, 0.2, 0.9],
                                colors: [Color(0xff9C9ED7), Color(0xff9C9ED7), Color(0xff7C7FC0),]
                            ),
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child:TextButton(
                            style: TextButton.styleFrom(
                              primary: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(40.0),
                              ),
                            ),
                            onPressed: () async {
                              await Navigator.push(context, CupertinoPageRoute(builder: (context) =>  NewTaskPage(date: date,)));
                              context.read<SingleDayPageBloc>().add(SingleDayPageStartEvent(date: date));
                              //context.read<WelcomePageBloc>().add(WelcomePageStartEvent());
                              //Navigator.push(context, CupertinoPageRoute(builder: (context) =>  AuthPage()));
                            },
                            child: Text("Додати завдання",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                          ),
                            ),
                        ),
                      ],
                    ),
                  );
                }


                return Container();

              },
            )
          ],
        ),
      ),
    );
  }
}
